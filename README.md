# voltages.me

Personal website for my projects and about myself.

I have rewritten this multiple times now. Made a seperate archive branch for a older version made in plain HTML, CSS and JS.
There used to be a branch with Bulma as the CSS framework but it was deleted and replaced with the archive branch.

## Dependencies

To install all the dependencies, run ``yarn`` or ``npm install``

-   TailwindCSS
-   styled-components (along with \_document.tsx for hot-refresh to work properly)
-   framer-motion
-   prettier
-   use-lanyard

## Credits

* [cnrad/next-template](https://github.com/cnrad/next-template)
    * Made making this way way better and smoother.
* [pxseu](https://github.com/pxseu)
    * For helpin out with certain parts of the website.

