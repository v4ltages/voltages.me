import { useRef } from "react";

export default function Quotes() {
    const Array = [
        "Check this out https://www.youtube.com/watch?v=dQw4w9WgXcQ",
        "Leaving and coming back with a fresh set of eyes always solves problems",
        "Don't let me near a server rack",
        "All the quotes are hidden under a very specific file, its not that hard to find",
        "This is a quote, it's not a very good one but it's a quote",
        "Its taken over a 100 commits to build this up",
        "It should work, it should but why is it not?!",
        "My projects are not a work of art, they are a work of passion",
        "Inspect the page, you might find something interesting",
        "Well, some projects i am kinda proud of",
        "What about a coffee? I could use one ☕️",
        "Does this part change?",
        "Yes, this part can change. Try refreshing the page",
        "A wee in the rain that will melt away into a little puddle that will evaporate into nothing",
        "When was the last time i rewrote this whole website?",
        "I could use a nice hot tea 🍵",
        "My brain is wired to be such a tangled mess 🧠",
        "Moo 🐄",
        "Ok",
        "I don’t know everything, I just know what i know",
        "You should listen to Holdin' On To Your Silence",
        "Anything that can go wrong will go wrong",
        "Nature is where you will find ur inner harmony 🌲",
        "I USE ARCH BTW.",
        "What you're referring to as Linux, is in fact, GNU/Linux, or as I've recently taken to calling it, GNU plus Linux",
        "What if you wrote /heart at the end of the url?",
        "Have you tried adding /holdin-on to the end of the url?",
        "Segmentation fault (core dumped)",
        "Oh no, something went wrong",
        "Oh no, anyway...",
        "Bailing out, you are on your own now. Good luck.",
        "Operation not permitted",
        "No such file or directory",
        "Out of memory",
        "Device or resource busy",
        "Network is unreachable",
        "403 Forbidden, did you check ur permissions?",
        "No route to host",
        "500 Internal Server Error, better get on that then?",
        "404 Where did all my quotes go?",
        "Have you been keeping a count of these?",
        "Any suggestions or ideas? You can send them to me at contact@voltages.me",
        "I was at #emfcamp2022 🏕",
        "Web design is something of a drug"
    ];
    const randomArray = useRef(Array[Math.floor(Math.random()*Array.length)]).current;
    return (
        <p className="sm:pl-4 font-['Fira_Sans'] font-normal text-xl text-[#6c6f85] dark:text-gray-300">{randomArray}</p>
    );
};