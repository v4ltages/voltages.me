import type { NextPage } from "next";
import Head from "next/head";
import { SiGithubsponsors, SiAnilist } from "react-icons/si";
import { HiOutlineKey } from "react-icons/hi2";

const Etc: NextPage = () => {
    return (
        <>
            <Head>
                <title>etc - Voltages</title>
            </Head>
            <div className="max-w-[64em] ml-auto mr-auto lg:pt-[10em] pb-[10em] px-8">
                <p className="font-[Rubik] font-normal text-2xl pb-2">/etc</p>
                <p className="font-['Fira_Sans'] text-[#6c6f85] dark:text-gray-300 text-2xl pb-2"><span className="font-bold text-[#ECCECE]">Thank you</span> for looking through my site.</p>
                <p className="font-[Rubik] font-normal text-2xl pt-2 pb-2">Contact</p>
                <p className="font-['Fira_Sans'] text-[#6c6f85] dark:text-gray-300 text-xl pb-2">Feel free to send me a email at <a className="text-[#D7B7EE] font-bold hover:underline" href="mailto:contact@voltages.me">contact@voltages.me</a> for any interests. If you want to check something else out then look at the Other section.</p>
                <p className="font-[Rubik] font-normal text-2xl pt-2 pb-2">Other</p>
                <p className="font-['Fira_Sans'] text-[#6c6f85] dark:text-gray-300 text-xl pb-4">If you want to take a look at the code for the site, then the source code is available on <a className="text-[#EDC4E5] font-bold hover:underline" href="https://gitlab.com/voltages/voltages.me">GitLab</a></p>
                <div className="font-['Fira_Sans'] flex gap-2 flex-col md:flex-row text-xl">
                    <a className="flex group flex-row p-3 dark:hover:outline-white hover:outline-[#6c6f85] outline outline-2 outline-transparent rounded-lg transition-all active:scale-[.96]" target="_blank" href="https://anilist.co/user/Voltages/" rel="noopener noreferrer">
                        <SiAnilist className="text-3xl"/>
                        <span className="pl-2 text-lg font-['Fira_Sans'] font-semibold">My Anilist</span>
                    </a>
                    <a className="flex group flex-row p-3 dark:hover:outline-white hover:outline-[#6c6f85] outline outline-2 outline-transparent rounded-lg transition-all active:scale-[.96]" target="_blank" href="https://github.com/sponsors/v4ltages" rel="noopener noreferrer">
                        <SiGithubsponsors className="text-3xl" />
                        <span className="pl-2 text-lg font-['Fira_Sans'] font-semibold">GitHub Sponsors</span>
                    </a>
                    <a className="flex group flex-row p-3 dark:hover:outline-white hover:outline-[#6c6f85] outline outline-2 outline-transparent rounded-lg transition-all active:scale-[.96]" target="_blank" href="https://gitlab.com/voltages.gpg" rel="noopener noreferrer">
                        <HiOutlineKey className="text-3xl" />
                        <span className="pl-2 text-lg font-['Fira_Sans'] font-semibold">My Public PGP</span>
                    </a>
                </div>
            </div>
        </>
    );
};

export default Etc;