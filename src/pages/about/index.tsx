import Head from "next/head";
import type { NextPage } from "next";
import { SiTailwindcss, SiNginx, SiDocker, SiReact, SiNextdotjs, SiMacos, SiWindows, SiArchlinux, SiUbuntu, SiDebian, SiSpotify } from "react-icons/si";

const About: NextPage = () => {
    return (
        <>
            <Head>
                <title>About - Voltages</title>
            </Head>
            <div>
                <div className="max-w-[64em] ml-auto mr-auto pt-24 pb-48 lg:pt-[10em] px-8">
                    <p className="font-[Rubik] font-normal text-2xl pb-2">/about</p>
                    <p className="font-['Fira_Sans'] text-[#6c6f85] dark:text-gray-300 text-xl pb-2">So, i&#39;m Voltages. Born and currently residing in Estonia. Graduated as a Junior IT-Specialist in 2021 and currently enrolled at a university to continue my studies into information technology. Tinkering & playing around with server's and software.</p>
                    <p className="font-['Fira_Sans'] text-[#6c6f85] dark:text-gray-300 text-xl pb-2">Occasionally might be a otaku, Try to create things, Run virtual machines, Administrate, Draw art and design aswell as have a tiny bit of a photography interest. Oh, i like tea, coffee and listen to alot of music on repeat.</p>
                    <p className="font-['Fira_Sans'] text-[#6c6f85] dark:text-gray-300 text-xl pb-2">Alot of what i&#39;ve gained has been self-taught. Big chunk of it is on web development.</p>
                    <s className="font-['Fira_Sans'] text-[#6c6f85] dark:text-gray-300 text-xl pb-2">So, Clearly web design is my passion...</s>
                    <p className="font-['Fira_Sans'] text-[#6c6f85] dark:text-gray-300 text-xl py-2">But in all honesty, i find technology interesting and all the ways it communicates with eachother. That's why i've developed a liking for development but also hardware itself.</p>
                    <div className="grid gap-0 sm:gap-4 grid-cols-1 sm:grid-cols-[4fr,2fr]">
                        <p className="font-['Fira_Sans'] text-[#6c6f85] dark:text-gray-300 text-xl pb-0 sm:pb-2">An example is this website. Made with React as the base framework with Next.js acting as the exporter with different functions and Tailwind CSS for styling. Use-lanyard uses Lanyard that grabs data from a Discord Bot and makes it easily accessible with there API. Making it possible to show what i'm currently doing. After exported, the website will be uploaded onto a Nginx instance running on a Virtual Private Server</p>
                        <div className="text-[#6c6f85] font-normal bg-[#dce0e8] dark:bg-[#302D40] rounded-xl my-4 sm:m-4 p-4 text-lg font-['Fira_Sans'] dark:text-gray-300 grid gap-4 grid-cols-4 sm:grid-cols-2 md:grid-cols-4 place-items-center">
                            <SiReact className="text-3xl"/>
                            <SiTailwindcss className="text-3xl"/>
                            <SiNextdotjs className="text-3xl"/>
                            <SiNginx className="text-3xl"/>
                            <SiWindows className="text-3xl"/>
                            <SiMacos className="text-3xl"/>
                            <SiArchlinux className="text-3xl"/>
                            <SiUbuntu className="text-3xl"/>
                            <SiDebian className="text-3xl"/>
                            <SiDocker className="text-3xl"/>
                            <SiSpotify className="text-3xl"/>
                        </div>
                    </div>
                    <div className="grid gap-0 sm:gap-4">
                        <p className="font-['Fira_Sans'] text-[#6c6f85] dark:text-gray-300 text-xl pb-2">I daily drive Windows, MacOS and Arch Linux. I do stick with Linux more as its considerably more free and open. I know what runs on it and how to manage the system as a whole. For servers i tend to stick with Ubuntu or Debian. Certain functionality from software i usually like to use Docker for. The ability to scale and for a idempotent system is what makes it great.</p>
                    </div>
                </div>
            </div>
        </>
    );
};

export default About;