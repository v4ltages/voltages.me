import type { NextApiRequest, NextApiResponse } from 'next'

export default function handler(
        req: NextApiRequest,
        res: NextApiResponse
    ) {
    let amountOfCows = Math.floor(Math.random() * 1000)
    let fieldOfCows = Array.from({length: amountOfCows}, () => '🐄').join('')
    res.setHeader('Location', 'https://cow.voltages.me')
    res.statusMessage = 'Too many cows'
    res.status(413).json(
        {
            "Status": "413 Payload Too Large",
            "⛰️": fieldOfCows,
            "💭": "Hmm, how many cows are here?",
            "➡️": "You should check the header content ❤️"
        }
    )
}